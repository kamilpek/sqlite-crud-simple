/* 
*  Prosty interkatywny porgram do obłsuig bazy danych 
*/
package crud;

import java.sql.*;
import java.io.*;
import java.util.Scanner;

/**
 * @author Kamil Pek 231050
 */

public class Crud {

    public static void main(String[] args) throws ClassNotFoundException {
        Connection polacz = null;
        Statement stat = null;
        int p = 0;
        String tak = "tak";
        Scanner in = new Scanner(System.in);
        
        // USTANAWIANIE POLACZENIA ZA BAZA DANYCH
        try{
            Class.forName("org.sqlite.JDBC");
            polacz = DriverManager.getConnection("jdbc:sqlite:test.db");
            stat = polacz.createStatement();
            stat.executeUpdate("DROP TABLE IF EXISTS ludzie;");
            System.out.println("Polaczono z baza!");
        } catch (SQLException e) {
            System.err.println("Wystapil problem: \n"+ e.getMessage());
            System.exit(0);
        }
        
        // TWORZENIE TABELI W BAZIE DANYCH
        String create_table = "CREATE TABLE IF NOT EXISTS ludzie (id INTEGER PRIMARY KEY AUTOINCREMENT, imie varchar(255), nazwisko varchar(255), telefon int)";
        try {
            stat.executeUpdate(create_table);
            System.out.println("Utworzono tabele w bazie danych.");            
        } catch (SQLException e){
            System.err.println("Wystąpił problem "+ e.getMessage() );
            System.exit(0);
        }
        
        // WPROWADZANIE REKORDÓW DO BAZY DANYCH
        System.out.println("\n WPROWADZANIE REKORDOW DO BAZY DANYCH");
        System.out.println("Prosze podac ilosc rekordow do wprowadzenie.");
        int rekordy = in.nextInt();
        int i = 0;
        while (i < rekordy){
            System.out.println("Prosze teraz podawac po kolei dane.");
            System.out.println("Prosze podac imie.");
            String imie = in.next();
            System.out.println("Prosze podac nazwisko.");
            String nazwisko = in.next();
            System.out.println("Prosze podac numer telefonu.");
            int telefon = in.nextInt();
            try {
                stat.executeUpdate("INSERT INTO ludzie (imie, nazwisko, telefon) VALUES ('"+imie+"', '"+nazwisko+"', "+telefon+");");
                System.out.println("Zadanie wykonane.");
            } catch (SQLException e){
            System.err.println("Wystąpił problem "+ e.getMessage() );
            System.exit(0);
            }
            i++;
        } // koniec while z insertem
                
        // WYSWIETLANIE ZAWARTOSCI TABELI ludzie
        System.out.println("\n WYSWIETLANIE ZAWARTOSCI TABELI.");
        try {
            ResultSet rs = stat.executeQuery("SELECT * FROM ludzie;");
            while (rs.next() ){
                int id = rs.getInt("id");
                String imie = rs.getString("imie");
                String nazwisko = rs.getString("nazwisko");
                int telefon = rs.getInt("telefon");
                System.out.println("ID = "+ id);
                System.out.println("Imie = "+ imie);
                System.out.println("Nazwisko = "+ nazwisko);
                System.out.println("Telefon = "+ telefon);
                System.out.println(" ");
            }            
        } catch (SQLException e) {
            System.err.println("Wystąpił problem "+ e.getMessage() );
            System.exit(0);
        }        
        
        System.out.println("Czy chiałbys zmienić dane w bazie?");
        String potwierdzenie1 = in.next();
        
        if (potwierdzenie1.equals("tak") || potwierdzenie1.equals("TAK") || potwierdzenie1.equals("Tak") || potwierdzenie1.equals("t") || potwierdzenie1.equals("T")){        
        // AKTUALIZOWANIE DANYCH W BAZIE
        System.out.println("\n AKTUALIZOWANIE DANYCH");
        p = 1;
        System.out.println("Prosze wybrac pole, ktore zamirzasz zmienic.");
        System.out.println("1. Imie. \n2. Nazwisko. \n3. Numer telefonu.");
        int j = in.nextInt();
        switch(j){
            case 1: {
                System.out.println("Wybrales zmiane imienia. Prosze podac w, ktorym rekordzie ma zostac wprowadzona zmiana.");
                int rekord = in.nextInt();
                System.out.println("Prosze podac nowe imie.");
                String noweimie = in.next();
                try {
                    stat.executeUpdate("UPDATE ludzie set imie='"+noweimie+"' where id ="+rekord+";");
                    System.out.println("Wykonano zmiane.");
                } catch (SQLException e){
                    System.err.println("Wystąpił problem "+ e.getMessage() );
                    System.exit(0);
                }
                break;
            }
            case 2: {
                System.out.println("Wybrales zmiane nazwiska. Prosze podac w, którym rekordzie ma zostac wprowadzona zmiana.");
                int rekord = in.nextInt();
                System.out.println("Prosze podac nowe imie.");
                String nowenazwisko = in.next();
                try { 
                    stat.executeUpdate("UPDATE ludzie set nazwisko='"+nowenazwisko+"' where id ="+rekord+";");
                    System.out.println("Wykonano zmiane.");
                } catch (SQLException e){
                    System.err.println("Wystąpił problem "+ e.getMessage() );
                    System.exit(0);
                }
                break;
            }
            case 3: {
                System.out.println("Wybrales zmiane numeru telefonu. Prosze podac w, którym rekordzie ma zostac wprowadzona zmiana.");
                int rekord = in.nextInt();
                System.out.println("Prosze podac nowy numer telefonu.");
                String nowytelefon = in.next();
                try { 
                    stat.executeUpdate("UPDATE ludzie set telefon='"+nowytelefon+"' where id ="+rekord+";");
                    System.out.println("Wykonano zmiane."); 
                } catch (SQLException e){
                System.err.println("Wystąpił problem "+ e.getMessage() );
                System.exit(0);
                }
                break;
            }
        } // koniec switcha z update
        } else { System.out.println("Zrezygnowales ze zmiany."); }
        
        System.out.println("Czy chcialbys usunac ktorys rekord z bazy?");
        String potwierdzenie2 = in.next();
        
        if(potwierdzenie2.equals("tak") || potwierdzenie2.equals("TAK") || potwierdzenie2.equals("Tak") || potwierdzenie2.equals("t") || potwierdzenie2.equals("T")){        
        
        // USUWANIE WYBRANYCH REKORDOW
        System.out.println("\n USUWANIE DANYCH");
        p = 2;
        System.out.println("Prosze podac numer rekordu do usuniecia");
        int rekord = in.nextInt();
        try {
            stat.executeUpdate("DELETE from ludzie WHERE ID="+ rekord +";");
            System.out.println("Zadanie wykonane.");
        } catch (SQLException e){
            System.err.println("Wystąpił problem "+ e.getMessage() );
            System.exit(0);
       }
       } else { System.out.println("Zrezygnowales z usuwania danych."); }
        
        if (p != 0){
                               
        // WYSWIETLANIE ZAWARTOSCI TABELI ludzie PO RAZ DRUGI
        System.out.println("\n WYSWIETLANIE ZAWARTOSCI TABELI PO ZATWIERDZENIU WSZYSTKICH ZMIAN.");
        try {
            ResultSet rs = stat.executeQuery("SELECT * FROM ludzie;");
            while (rs.next() ){
                int id = rs.getInt("id");
                String imie = rs.getString("imie");
                String nazwisko = rs.getString("nazwisko");
                int telefon = rs.getInt("telefon");
                System.out.println("ID = "+ id);
                System.out.println("Imie = "+ imie);
                System.out.println("Nazwisko = "+ nazwisko);
                System.out.println("Telefon = "+ telefon);
                System.out.println(" ");
            }            
        } catch (SQLException e) {
            System.err.println("Wystąpił problem "+ e.getMessage() );
            System.exit(0);
        } } else { System.out.println("Dziekuje za skorzystanie z programu."); }
        
    } // koniec maina    
} // koniec klasy crud
